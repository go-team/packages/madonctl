module github.com/McKael/madonctl

require (
	github.com/McKael/madon/v2 v2.3.1-0.20220823202755-c45cd336c53a
	github.com/ghodss/yaml v1.0.0
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/kr/text v0.2.0
	github.com/mattn/go-isatty v0.0.16
	github.com/pelletier/go-toml/v2 v2.0.3 // indirect
	github.com/pkg/errors v0.9.1
	github.com/spf13/afero v1.9.2 // indirect
	github.com/spf13/cobra v1.5.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.12.0
	github.com/stretchr/testify v1.8.0
	github.com/subosito/gotenv v1.4.1 // indirect
	golang.org/x/net v0.0.0-20220822230855-b0a4917ee28c
	golang.org/x/sys v0.0.0-20220818161305-2296e01440c6 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)

go 1.13
